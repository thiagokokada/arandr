{ lib
, python3
, docutils
, gobject-introspection
, gsettings-desktop-schemas
, gtk3
, wrapGAppsHook
, xorg
}:

with python3.pkgs;
buildPythonApplication {
  pname = "arandr";
  version = "dev";

  strictDeps = false;

  buildInputs = [ docutils gsettings-desktop-schemas gtk3 ];
  nativeBuildInputs = [ gobject-introspection wrapGAppsHook ];
  propagatedBuildInputs = [ xorg.xrandr pygobject3 ];

  src = ./.;

  meta = {
    homepage = "https://christian.amsuess.com/tools/arandr/";
    description = "A simple visual front end for XRandR";
    license = lib.licenses.gpl3;
  };
}
